from flask import Flask

app = Flask(__name__)


@app.route('/vacancy/', methods=['GET', 'POST'])  # paragraph 1 in functionality
def all_vacancies():
    return "All vacancies"


@app.route('/vacancy/<id>/', methods=['GET', 'PUT', 'DELETE'])  # paragraph 2 in functionality
def vacancy_info():
    return "Vacancy info"


@app.route('/vacancy/<id>/events/', methods=['GET', 'POST'])  # paragraph 3 in functionality
def all_events_for_vacancy():
    return "All events for vacancy"


@app.route('/vacancy/<id>/events/<event_id>/', methods=['GET', 'PUT', 'DELETE'])  # paragraph 4 in functionality
def event_info():
    return "Event info"


@app.route('/vacancy/<id>/history/', methods=['GET'])  # paragraph 5 in functionality
def vacancy_history():
    return "Vacancy history"


@app.route('/user/', methods=['GET'])
def user_main_page():
    return "User main page"


@app.route('/user/calendar/', methods=['GET'])  # paragraph 6 in functionality
def user_calendar():
    return "User calendar"


@app.route('/user/mail/', methods=['GET'])  # paragraph 7 in functionality
def user_mail():
    return "User mail"


@app.route('/user/settings/', methods=['GET', 'PUT'])
def user_settings():
    return "User settings"


@app.route('/user/documents/', methods=['GET', 'POST'])  # paragraph 8 in functionality
def user_documents():
    return "User documents"


@app.route('/user/documents/<document_id>/', methods=['GET', 'PUT', 'DELETE'])  # p8
def document_content():
    return "Document content"


@app.route('/user/templates/', methods=['GET', 'POST'])  # paragraph 9 in functionality
def user_templates():
    return "User templates"


@app.route('/user/templates/<template_id>/', methods=['GET', 'PUT', 'DELETE'])  # p9
def template_content():
    return "Template content"


if __name__ == '__main__':
    app.run()
