from flask import Flask
from flask import request, render_template

import datetime
import db_processing

app = Flask(__name__)


@app.route('/vacancy/', methods=['GET', 'POST', 'PUT'])
def show_all_vacancies():
    if request.method == 'POST':
        vacancy_data = {'user_id': 1,
                        'creation_date': datetime.date.today(),
                        'status': 0,
                        'company': request.form.get('company'),
                        'contacts_ids': request.form.get('contacts_ids'),
                        'description': request.form.get('description'),
                        'position_name': request.form.get('position_name'),
                        'comment': request.form.get('comment')}
        db_processing.insert_info('vacancies', vacancy_data)
        return "Successfully added!"
    elif request.method == 'PUT':
        pass
    else:
        res = db_processing.select_info('SELECT * FROM vacancies', {})
        result = ""
        for item in res:
            result += "<tr>"
            for value in item:
                result += "<td>" + str(value) + "</td>"
            result += "</tr>"
        return render_template('new_vacancies.html', vacancies=result)


@app.route('/vacancy/<vacancy_id>/', methods=['GET', 'PUT', 'DELETE'])
def vacancy_info(vacancy_id):
    if request.method == 'PUT':
        pass
    elif request.method == 'DELETE':
        pass
    else:
        response = db_processing.select_info('SELECT * FROM vacancies WHERE vacancy_id = :id', {"id": vacancy_id})
        result = ""
        for item in response:
            result += "<tr>"
            for value in item:
                result += "<td>" + str(value) + "</td>"
            result += "</tr>"
        return render_template('new_vacancies.html', vacancies=result)


@app.route('/vacancy/<vacancy_id>/events/', methods=['GET', 'POST'])
def all_events_for_vacancy(vacancy_id):
    if request.method == 'POST':
        event_data = {'event_date': request.form.get('event_date'),
                      'vacancy_id': request.form.get('vacancy_id'),
                      'title': request.form.get('title'),
                      'description': request.form.get('description'),
                      'due_to_date': request.form.get('due_to_date'),
                      'status': 0}
        db_processing.insert_info('events', event_data)
        return "Successfully added!"
    else:
        result = db_processing.select_info('SELECT * FROM events WHERE vacancy_id = :id', {"id": vacancy_id})
        res = ""
        for item in result:
            res += "<tr>"
            for value in item:
                res += "<td>" + str(value) + "</td>"
            res += "</tr>"
        return render_template('new_events.html', events=res)


@app.route('/vacancy/<vacancy_id>/events/<event_id>/', methods=['GET', 'PUT', 'DELETE'])
def event_info(vacancy_id, event_id):
    if request.method == 'PUT':
        pass
    elif request.method == 'DELETE':
        pass
    else:
        response = db_processing.select_info('SELECT * FROM events WHERE event_id = :id', {"id": event_id})
        result = ""
        for item in response:
            result += "<tr>"
            for value in item:
                result += "<td>" + str(value) + "</td>"
            result += "</tr>"
        return render_template('new_events.html', events=result)


@app.route('/vacancy/<vacancy_id>/history/', methods=['GET'])  # paragraph 5 in functionality
def vacancy_history():
    return "Vacancy history"


@app.route('/user/', methods=['GET'])
def user_main_page():
    return "User main page"


@app.route('/user/calendar/', methods=['GET'])  # paragraph 6 in functionality
def user_calendar():
    return "User calendar"


@app.route('/user/mail/', methods=['GET'])  # paragraph 7 in functionality
def user_mail():
    return "User mail"


@app.route('/user/settings/', methods=['GET', 'PUT'])
def user_settings():
    return "User settings"


@app.route('/user/documents/', methods=['GET', 'POST'])  # paragraph 8 in functionality
def user_documents():
    return "User documents"


@app.route('/user/documents/<document_id>/', methods=['GET', 'PUT', 'DELETE'])  # p8
def document_content():
    return "Document content"


@app.route('/user/templates/', methods=['GET', 'POST'])  # paragraph 9 in functionality
def user_templates():
    return "User templates"


@app.route('/user/templates/<template_id>/', methods=['GET', 'PUT', 'DELETE'])  # p9
def template_content():
    return "Template content"


@app.route('/')
def main_page():
    return "Welcome to CRM, coming soon"


if __name__ == '__main__':
    app.run()
