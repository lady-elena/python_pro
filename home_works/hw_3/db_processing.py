import sqlite3

db_name = 'crm.db'


def select_info(sql_query, params):
    conn = sqlite3.connect(db_name)
    cur = conn.cursor()
    cur.execute(sql_query, params)
    result = cur.fetchall()
    conn.close()
    return result


def insert_info(table_name, data):
    columns = ','.join(data.keys())
    placeholders = ':' + ', :'.join(data.keys())
    query = 'INSERT INTO %s (%s) VALUES (%s)' % (table_name, columns, placeholders)
    conn = sqlite3.connect(db_name)
    cur = conn.cursor()
    cur.execute(query, data)
    conn.commit()
    conn.close()
