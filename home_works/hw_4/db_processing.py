import sqlite3

db_name = 'crm.db'


class DB:
    def __enter__(self):
        self.conn = sqlite3.connect(db_name)
        self.cur = self.conn.cursor()
        return self

    def query(self, qry):
        self.cur.execute(qry)
        result = self.cur.fetchall()
        return result

    def insert_info(self, table_name, data):
        columns = ','.join(data.keys())
        placeholders = ':' + ', :'.join(data.keys())
        query = 'INSERT INTO %s (%s) VALUES (%s)' % (table_name, columns, placeholders)
        self.cur.execute(query, data)
        self.conn.commit()

    def update_vacancy(self, table_name, data):
        query = 'UPDATE vacancies SET position_name = :position_name, company = :company, description = :description, contacts_ids = :contacts_ids, comment = :comment WHERE vacancy_id = :vacancy_id;'
        self.cur.execute(query, data)
        self.conn.commit()

    def update_event(self, table_name, data):
        query = 'UPDATE events SET description = :description, event_date = :event_date, title = :title, due_to_date = :due_to_date, status = :status WHERE event_id = :event_id;'
        self.cur.execute(query, data)
        self.conn.commit()

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.cur.close()
        self.conn.close()
