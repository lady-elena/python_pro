from flask import Flask
from flask import request, render_template
import alchemy_db
from models import Vacancy, Event

app = Flask(__name__)


@app.route('/vacancy/', methods=['GET', 'POST'])
def show_all_vacancies():
    alchemy_db.init_db()

    if request.method == 'POST':
        company = request.form.get('company')
        contacts_ids = request.form.get('contacts_ids')
        description = request.form.get('description')
        position_name = request.form.get('position_name')
        comment = request.form.get('comment')

        current_vacancy = Vacancy(0, company, contacts_ids, description, position_name, comment, 1)
        alchemy_db.db_session.add(current_vacancy)
        alchemy_db.db_session.commit()

    all_vacancies = alchemy_db.db_session.query(Vacancy.vacancy_id, Vacancy.creation_date, Vacancy.status,
                                                Vacancy.company, Vacancy.contacts_ids, Vacancy.description,
                                                Vacancy.position_name, Vacancy.comment).all()
    return render_template('all_vacancies.html', vacancies=all_vacancies)


@app.route('/vacancy/<int:vacancy_id>/', methods=['GET', 'PUT', 'DELETE'])
def vacancy_info(vacancy_id):
    alchemy_db.init_db()

    company = request.form.get('company')
    contacts_ids = request.form.get('contacts_ids')
    description = request.form.get('description')
    position_name = request.form.get('position_name')
    comment = request.form.get('comment')

    if request.method == 'PUT':
        edited_vacancy = alchemy_db.db_session.query(Vacancy).filter(Vacancy.vacancy_id == vacancy_id).first()
        edited_vacancy.company = company
        edited_vacancy.contacts_ids = contacts_ids
        edited_vacancy.description = description
        edited_vacancy.position_name = position_name
        edited_vacancy.comment = comment
        alchemy_db.db_session.commit()

    vacancy = alchemy_db.db_session.query(Vacancy.vacancy_id, Vacancy.creation_date, Vacancy.status, Vacancy.company,
                                          Vacancy.contacts_ids, Vacancy.description, Vacancy.position_name,
                                          Vacancy.comment, Vacancy.user_id).all()[vacancy_id - 1]
    return render_template('for_vacancies.html', vacancies=vacancy, vacancy_id=vacancy_id)


@app.route('/vacancy/<int:vacancy_id>/events/', methods=['GET', 'POST'])
def all_events_for_vacancy(vacancy_id):
    alchemy_db.init_db()
    if request.method == 'POST':
        vacancy_id = request.form.get('vacancy_id')
        description = request.form.get('description')
        event_date = request.form.get('event_date')
        title = request.form.get('title')
        due_to_date = request.form.get('due_to_date')
        status = 0

        current_event = Event(vacancy_id, description, event_date, title, due_to_date, status)
        alchemy_db.db_session.add(current_event)
        alchemy_db.db_session.commit()

    all_events = alchemy_db.db_session.query(Event.event_id, Event.vacancy_id, Event.description, Event.event_date, Event.title, Event.due_to_date, Event.status).filter_by(vacancy_id=vacancy_id).all()
    return render_template('new_events.html', events=all_events, vacancy_id=vacancy_id)


@app.route('/vacancy/<int:vacancy_id>/events/<int:event_id>/', methods=['GET', 'PUT', 'DELETE'])
def event_info(vacancy_id, event_id):
    alchemy_db.init_db()

    description = request.form.get('description')
    event_date = request.form.get('event_date')
    title = request.form.get('title')
    due_to_date = request.form.get('due_to_date')

    if request.method == 'PUT':
        edited_event = alchemy_db.db_session.query(Event).filter(Event.event_id == event_id).first()
        edited_event.description = description
        edited_event.event_date = event_date
        edited_event.title = title
        edited_event.due_to_date = due_to_date

        alchemy_db.db_session.commit()

    event = alchemy_db.db_session.query(Event.event_id, Event.vacancy_id, Event.description, Event.event_date, Event.title, Event.due_to_date, Event.status).all()[event_id-1]
    return render_template('for_events.html', events=event, vacancy_id=vacancy_id, event_id=event_id)


@app.route('/vacancy/<vacancy_id>/history/', methods=['GET'])  # paragraph 5 in functionality
def vacancy_history():
    return "Vacancy history"


@app.route('/user/', methods=['GET'])
def user_main_page():
    return "User main page"


@app.route('/user/calendar/', methods=['GET'])  # paragraph 6 in functionality
def user_calendar():
    return "User calendar"


@app.route('/user/mail/', methods=['GET'])  # paragraph 7 in functionality
def user_mail():
    return "User mail"


@app.route('/user/settings/', methods=['GET', 'PUT'])
def user_settings():
    return "User settings"


@app.route('/user/documents/', methods=['GET', 'POST'])  # paragraph 8 in functionality
def user_documents():
    return "User documents"


@app.route('/user/documents/<document_id>/', methods=['GET', 'PUT', 'DELETE'])  # p8
def document_content():
    return "Document content"


@app.route('/user/templates/', methods=['GET', 'POST'])  # paragraph 9 in functionality
def user_templates():
    return "User templates"


@app.route('/user/templates/<template_id>/', methods=['GET', 'PUT', 'DELETE'])  # p9
def template_content():
    return "Template content"


@app.route('/')
def main_page():
    return "Welcome to CRM, coming soon"


if __name__ == '__main__':
    app.run()
