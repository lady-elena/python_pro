from datetime import datetime
from sqlalchemy import Column, Integer, String, ForeignKey, Text, Date
from alchemy_db import Base


class User(Base):
    __tablename__ = 'users'
    user_id = Column(Integer, primary_key=True, nullable=False, unique=True)
    name = Column(String(50), nullable=False, unique=True)
    login = Column(String(50), nullable=False, unique=True)
    password = Column(String(50), nullable=False)
    email = Column(String(120), nullable=False, unique=True)

    def __init__(self, name=None, email=None, password=None):
        self.name = name
        self.email = email
        self.password = password

    def __repr__(self):
        return '<User %r>' % self.name


class EmailCreds(Base):
    __tablename__ = 'emailCreds'
    id = Column(Integer, primary_key=True, nullable=False, unique=True, autoincrement=True)
    user_id = Column(Integer, ForeignKey('users.user_id'), nullable=False, unique=True)
    email = Column(String(120), nullable=False, unique=True)
    login = Column(String(50), nullable=False, unique=True)
    password = Column(String(50), nullable=False)
    pop_server = Column(String(120), nullable=False)
    smtp_server = Column(String(120), nullable=False)

    def __init__(self, login=None, password=None):
        self.login = login
        self.password = password

    def __repr__(self):
        return '<EmailCreds %r>' % self.email


class Vacancy(Base):
    __tablename__ = 'vacancies'
    vacancy_id = Column(Integer, primary_key=True, nullable=False, autoincrement=True)
    creation_date = Column(Date, default=datetime.now())
    status = Column(Integer, nullable=False)
    company = Column(String(50), nullable=False)
    contacts_ids = Column(String(120))
    description = Column(String(200), nullable=False)
    position_name = Column(String(120), nullable=False)
    comment = Column(String(120), nullable=True)
    user_id = Column(Integer, ForeignKey('users.user_id'), nullable=False)

    def __init__(self, status, company, contacts_ids, description, position_name, comment, user_id):
        self.status = status
        self.company = company
        self.contacts_ids = contacts_ids
        self.description = description
        self.position_name = position_name
        self.comment = comment
        self.user_id = user_id

    def __repr__(self):
        return '<Vacancy %r>' % self.position_name


class Event(Base):
    __tablename__ = 'events'
    event_id = Column(Integer, primary_key=True, nullable=False, unique=True, autoincrement=True)
    vacancy_id = Column(Integer, nullable=False)
    description = Column(String(200))
    event_date = Column(String(50))
    title = Column(String(50))
    due_to_date = Column(String(50))
    status = Column(Integer)

    def __init__(self, vacancy_id, description, event_date, title, due_to_date, status):
        self.vacancy_id = vacancy_id
        self.title = title
        self.description = description
        self.event_date = event_date
        self.due_to_date = due_to_date
        self.status = status

    def __repr__(self):
        return '<Event %r>' % self.title


class Template(Base):
    __tablename__ = 'templates'
    id = Column(Integer, primary_key=True, nullable=False, autoincrement=True)
    name = Column(String(50), nullable=False)
    content = Column(Text, nullable=False)
    user_id = Column(Integer, ForeignKey('users.user_id'), nullable=False)

    def __init__(self, name=None, content=None):
        self.name = name
        self.content = content

    def __repr__(self):
        return '<Template %r>' % self.name


class Document(Base):
    __tablename__ = 'documents'
    id = Column(Integer, primary_key=True, nullable=False, autoincrement=True)
    name = Column(String(50), nullable=False)
    description = Column(String(200), nullable=False)
    content = Column(Text, nullable=False)
    user_id = Column(Integer, ForeignKey('users.user_id'), nullable=False)

    def __init__(self, name=None, description=None, content=None):
        self.name = name
        self.description = description
        self.content = content

    def __repr__(self):
        return '<Document %r>' % self.name
