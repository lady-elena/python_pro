from flask import Flask

app = Flask(__name__)

all_vacancies = [
    {'vacancy_id': '1',
     'creation_date': '12.01.2023',
     'status': 1,
     'company': 'Lima Co',
     'contacts_ids': [1, 2],
     'description': 'Work on projects that allow you to use cutting edge tech. We believe in constantly evolving your mastery',
     'position_name': 'Python Engineer',
     'comment': 'Fluent English is required',
     'user_id': '1'},

    {'vacancy_id': '2',
     'creation_date': '17.01.2023',
     'status': 1,
     'company': 'Scopic',
     'contacts_ids': [3, 4],
     'description': 'Looking for python developer with expertise in Flask or similar framework (e.g. Django), understanding of cloud services such as AWS, GCP, Azure',
     'position_name': 'Remote Python Developer',
     'comment': 'This is an hourly paid position',
     'user_id': '1'},

    {'vacancy_id': '3',
     'creation_date': '18.01.2023',
     'status': 1,
     'company': 'Ascendeum Private Ltd',
     'contacts_ids': [5, 6],
     'description': 'We are looking for analytical and enthusiastic “Automation Engineer” who holds expertise and passion in designing, developing and implementing innovative automation solutions',
     'position_name': 'Python Automation Engineer',
     'comment': 'Fully remote (permanent)',
     'user_id': '1'},

    {'vacancy_id': '4',
     'creation_date': '20.01.2023',
     'status': 1,
     'company': 'InformData',
     'contacts_ids': [7, 8],
     'description': 'Looking for python dev, who will be responsible for the entire software development life cycle, debugging applications and configuring existing systems',
     'position_name': 'Python Dev',
     'comment': 'Remote, full time',
     'user_id': '1'}
]

all_events = [
    {'event_id': '1',
     'vacancy_id': '1',
     'description': 'Skype call with HR',
     'event_date': '20.02.2023',
     'title': 'Call',
     'dut_to_date': '20.02.2023',
     'status': 1},

    {'event_id': '2',
     'vacancy_id': '1',
     'description': 'Tech task',
     'event_date': '22.02.2023',
     'title': 'Test',
     'dut_to_date': '27.02.2023',
     'status': 1},

    {'event_id': '3',
     'vacancy_id': '2',
     'description': 'Zoom call with HR',
     'event_date': '08.02.2023',
     'title': 'Call',
     'dut_to_date': '08.02.2023',
     'status': 1},

    {'event_id': '4',
     'vacancy_id': '3',
     'description': 'Meeting with tech team',
     'event_date': '10.02.2023',
     'title': 'Meeting',
     'dut_to_date': '10.02.2023',
     'status': 1},

    {'event_id': '5',
     'vacancy_id': '4',
     'description': 'Interview with HR',
     'event_date': '17.02.2023',
     'title': 'Interview',
     'dut_to_date': '17.02.2023',
     'status': 1}
]


@app.route('/vacancy/', methods=['GET', 'POST'])  # paragraph 1 in functionality
def show_all_vacancies():
    return all_vacancies


@app.route('/vacancy/<vacancy_id>/', methods=['GET', 'PUT', 'DELETE'])  # paragraph 2 in functionality
def vacancy_info(vacancy_id):
    for vacancy in all_vacancies:
        if vacancy['vacancy_id'] == vacancy_id:
            return vacancy


@app.route('/vacancy/<vacancy_id>/events/', methods=['GET', 'POST'])  # paragraph 3 in functionality
def all_events_for_vacancy(vacancy_id):
    events_list = []
    for event in all_events:
        if event['vacancy_id'] == vacancy_id:
            events_list.append(event)
    return events_list


@app.route('/vacancy/<vacancy_id>/events/<event_id>/', methods=['GET', 'PUT', 'DELETE'])  # paragraph 4 in functionality
def event_info(vacancy_id, event_id):
    for event in all_events:
        if event['event_id'] == event_id:   # and event['vacancy_id'] == vacancy_id
            return event


@app.route('/vacancy/<vacancy_id>/history/', methods=['GET'])  # paragraph 5 in functionality
def vacancy_history():
    return "Vacancy history"


@app.route('/user/', methods=['GET'])
def user_main_page():
    return "User main page"


@app.route('/user/calendar/', methods=['GET'])  # paragraph 6 in functionality
def user_calendar():
    return "User calendar"


@app.route('/user/mail/', methods=['GET'])  # paragraph 7 in functionality
def user_mail():
    return "User mail"


@app.route('/user/settings/', methods=['GET', 'PUT'])
def user_settings():
    return "User settings"


@app.route('/user/documents/', methods=['GET', 'POST'])  # paragraph 8 in functionality
def user_documents():
    return "User documents"


@app.route('/user/documents/<document_id>/', methods=['GET', 'PUT', 'DELETE'])  # p8
def document_content():
    return "Document content"


@app.route('/user/templates/', methods=['GET', 'POST'])  # paragraph 9 in functionality
def user_templates():
    return "User templates"


@app.route('/user/templates/<template_id>/', methods=['GET', 'PUT', 'DELETE'])  # p9
def template_content():
    return "Template content"


if __name__ == '__main__':
    app.run()
